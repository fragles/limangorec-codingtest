<?php

declare(strict_types=1);

namespace App\Machine\Exception;

use Throwable;

class NoChangeAvailableException extends \Exception
{
    public function __construct(float $provided_amount, Throwable $previous = null)
    {
        $message = sprintf(
            'Impossible to return change of %01.2f€',
            $provided_amount
        );

        parent::__construct($message, 500, $previous);
    }
}
