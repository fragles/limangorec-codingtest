<?php

declare(strict_types=1);

namespace App\Machine\Exception;

use Throwable;

class InvalidItemQuantityException extends \Exception
{
    public function __construct(Throwable $previous = null)
    {
        $message = 'Invalid item quantity.';

        parent::__construct($message, 500, $previous);
    }
}
