<?php

declare(strict_types=1);

namespace App\Machine\Exception;

use Throwable;

class InsufficientAmountException extends \Exception
{
    public function __construct(float $provided_amount, float $required_amount, Throwable $previous = null)
    {
        $message = sprintf(
            'Provided insufficient amount (%01.2f€). Required %01.2f€',
            $provided_amount,
            $required_amount
        );

        parent::__construct($message, 500, $previous);
    }
}
