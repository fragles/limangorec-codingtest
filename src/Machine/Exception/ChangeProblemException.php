<?php

declare(strict_types=1);

namespace App\Machine\Exception;

class ChangeProblemException extends \Exception
{
    public function __construct(string $message, \Throwable $previous = null)
    {
        parent::__construct($message, 500, $previous);
    }
}
