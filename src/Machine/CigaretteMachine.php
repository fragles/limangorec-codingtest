<?php

declare(strict_types=1);

namespace App\Machine;

use App\Machine\Descriptor\Coin;
use App\Machine\Entity\PurchasedItem;
use App\Machine\Contract\MachineInterface;
use App\Machine\Contract\PurchasedItemInterface;
use App\Machine\Exception\NoChangeAvailableException;
use App\Machine\Contract\PurchaseTransactionInterface;
use App\Machine\Exception\InsufficientAmountException;
use App\Machine\Service\ChangeMakingProblemGreedyService;
use App\Machine\Contract\ChangeMakingProblemServiceInterface;

class CigaretteMachine implements MachineInterface
{
    public const ITEM_PRICE = 4.99;

    private ChangeMakingProblemServiceInterface $change_making_problem_service;

    public function __construct()
    {
        $this->change_making_problem_service = new ChangeMakingProblemGreedyService();
    }

    public function execute(PurchaseTransactionInterface $purchase_transaction): PurchasedItemInterface
    {
        $transaction_value = $this->calculateTransactionValue($purchase_transaction);

        $this->validateTransactionValue($purchase_transaction, $transaction_value);

        $change = $this->calculateChange($purchase_transaction, $transaction_value);

        $change_values = $this->calculateChangeValues($change);

        return new PurchasedItem(
            $purchase_transaction->getItemQuantity(),
            $transaction_value,
            $change_values
        );
    }

    private function calculateTransactionValue(PurchaseTransactionInterface $purchase_transaction): int
    {
        // return value as integer in euro cents
        return (int) ($purchase_transaction->getItemQuantity() * self::ITEM_PRICE * 100);
    }

    private function validateTransactionValue(PurchaseTransactionInterface $purchase_transaction, int $transaction_value): void
    {
        if ($purchase_transaction->getPaidAmount() < $transaction_value) {
            throw new InsufficientAmountException($purchase_transaction->getPaidAmount() / 100, $transaction_value / 100);
        }
    }

    private function calculateChange(PurchaseTransactionInterface $purchase_transaction, int $transaction_value): int
    {
        return $purchase_transaction->getPaidAmount() - $transaction_value;
    }

    private function calculateChangeValues(int $change): array
    {
        try {
            $change_value = $this->change_making_problem_service->solve(Coin::getEuroCents(), $change);

            $result = [];
            foreach ($change_value as $key => $value) {
                $result[] = [
                    $key / 100,
                    $value,
                ];
            }

            return $result;
        } catch (NoChangeAvailableException $exception) {
            throw new NoChangeAvailableException($change * 100, $exception);
        }
    }
}
