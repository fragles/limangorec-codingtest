<?php

declare(strict_types=1);

namespace App\Machine\Descriptor;

class Coin
{
    public const EURO_CENTS_RATIO = 100;

    public const COINS = [
        0.01,
        0.02,
        0.05,
        0.10,
        0.20,
        0.50,
        1,
        2,
        // TODO: from there are banknotes, should be used?
        //        5,
        //        10,
        //        20,
        //        50,
        //        100,
        //        200,
        //        // TODO: 500 EUR was discontinued but it is still available in use
        //        500,
    ];

    public static function getEuroCents(): array
    {
        return array_map(static function (float $value) {
            return $value * static::EURO_CENTS_RATIO;
        }, static::COINS);
    }

    public static function getEuros(): array
    {
        return static::COINS;
    }
}
