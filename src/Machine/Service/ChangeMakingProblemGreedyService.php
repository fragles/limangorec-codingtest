<?php

declare(strict_types=1);

namespace App\Machine\Service;

use App\Machine\Exception\ChangeProblemException;
use App\Machine\Contract\ChangeMakingProblemServiceInterface;

class ChangeMakingProblemGreedyService implements ChangeMakingProblemServiceInterface
{
    public function solve(array $coins, int $amount): array
    {
        if (!$coins) {
            throw new ChangeProblemException('Can not return without coins.');
        }

        $results = $this->makeResultArray($coins);

        return $this->calculateResults($results, $coins, $amount);
    }

    private function makeResultArray(array $available_coins): array
    {
        return array_combine(
            $available_coins,
            array_fill(0, count($available_coins), 0)
        );
    }

    private function calculateResults(array $results, array $coins, int $amount): array
    {
        $i = count($coins) - 1;

        while ($amount > 0) {
            $coin = $coins[$i];
            if (!$coin) {
                throw new ChangeProblemException('Can not return in coins for provided amount.');
            }

            $number_of_coins = floor($amount / $coin);
            $results[$coin] = $number_of_coins;
            $amount -= $number_of_coins * $coin;

            --$i;
        }

        return $results;
    }
}
