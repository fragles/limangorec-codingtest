<?php

declare(strict_types=1);

namespace App\Machine\Contract;

interface ChangeMakingProblemServiceInterface
{
    public function solve(array $coins, int $amount): array;
}
