<?php

declare(strict_types=1);

namespace App\Machine\Contract;

interface MachineInterface
{
    public function execute(PurchaseTransactionInterface $purchase_transaction): PurchasedItemInterface;
}
