<?php

declare(strict_types=1);

namespace App\Machine\Contract;

interface PurchaseTransactionInterface
{
    public function getItemQuantity(): int;

    public function getPaidAmount(): int;
}
