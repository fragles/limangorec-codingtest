<?php

declare(strict_types=1);

namespace App\Machine\Entity;

use App\Machine\Contract\PurchasedItemInterface;

class PurchasedItem implements PurchasedItemInterface
{
    protected int $item_quantity;
    protected float $total_amount;
    protected array $change;

    public function __construct(int $item_quantity, int $total_amount, array $change)
    {
        $this->item_quantity = $item_quantity;
        $this->total_amount = $total_amount / 100;
        $this->change = $change;
    }

    public function getItemQuantity(): int
    {
        return $this->item_quantity;
    }

    public function getTotalAmount(): float
    {
        return $this->total_amount;
    }

    public function getChange(): array
    {
        return $this->change;
    }
}
