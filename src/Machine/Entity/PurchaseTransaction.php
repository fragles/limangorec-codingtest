<?php

declare(strict_types=1);

namespace App\Machine\Entity;

use App\Machine\Contract\PurchaseTransactionInterface;
use App\Machine\Exception\InvalidItemQuantityException;

class PurchaseTransaction implements PurchaseTransactionInterface
{
    protected int $item_quantity;
    protected int $paid_amount;

    public function __construct(int $item_quantity, float $paid_amount)
    {
        if ($item_quantity <= 0) {
            throw new InvalidItemQuantityException();
        }

        $this->item_quantity = $item_quantity;
        // TODO: converting into euro cents but maybe should be made be represented by value object?
        $this->paid_amount = (int) ($paid_amount * 100);
    }

    public function getItemQuantity(): int
    {
        return $this->item_quantity;
    }

    public function getPaidAmount(): int
    {
        return $this->paid_amount;
    }
}
