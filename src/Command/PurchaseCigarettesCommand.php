<?php

declare(strict_types=1);

namespace App\Command;

use App\Machine\CigaretteMachine;
use App\Machine\Entity\PurchaseTransaction;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Command\Command;
use App\Machine\Contract\PurchasedItemInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PurchaseCigarettesCommand extends Command
{
    protected function configure(): void
    {
        $this->addArgument('packs', InputArgument::REQUIRED, 'How many packs do you want to buy?');
        $this->addArgument('amount', InputArgument::REQUIRED, 'The amount in euro.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $item_count = (int) $input->getArgument('packs');
            $amount = (float) \str_replace(',', '.', $input->getArgument('amount'));

            $purchase_transaction = new PurchaseTransaction($item_count, $amount);

            $cigarette_machine = new CigaretteMachine();
            $purchased_item = $cigarette_machine->execute($purchase_transaction);

            $this->displaySummary($output, $purchased_item);
            $this->displayChangeTable($output, $purchased_item);

            return self::SUCCESS;
        } catch (\Throwable $throwable) {
            $output->writeln('<error>Error occurred:</error>');
            $output->writeln(sprintf(
                '<error>%s</error>',
                $throwable->getMessage()
            ));

            return self::FAILURE;
        }
    }

    protected function displaySummary(OutputInterface $output, PurchasedItemInterface $purchased_item): void
    {
        $output->writeln(sprintf(
            'You bought <info>%d</info> packs of cigarettes for <info>%.2f€</info>, each for <info>%.2f€</info>. ',
            $purchased_item->getItemQuantity(),
            $purchased_item->getTotalAmount(),
            CigaretteMachine::ITEM_PRICE
        ));
    }

    protected function displayChangeTable(OutputInterface $output, PurchasedItemInterface $purchased_item): void
    {
        $output->writeln('Your change is:');

        $table = new Table($output);
        $table
            ->setHeaders(['Coins', 'Count'])
            ->setRows($purchased_item->getChange())
        ;

        $table->render();
    }
}
