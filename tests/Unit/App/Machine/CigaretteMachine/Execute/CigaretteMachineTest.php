<?php

declare(strict_types=1);

namespace App\Tests\Unit\App\Machine\CigaretteMachine\Execute;

use PHPUnit\Framework\TestCase;
use App\Machine\CigaretteMachine;

/**
 * @internal
 * @coversNothing
 */
class CigaretteMachineTest extends TestCase
{
    use TestTrait;

    /**
     * @dataProvider validEntryDataProvider
     *
     * @test
     */
    public function calculateChangeValues(int $item_quantity, float $paid_amount): void
    {
        // GIVEN
        $purchase_transaction = $this->mockPurchaseTransaction($item_quantity, $paid_amount);
        $cigarette_machine = new CigaretteMachine();

        // WHEN
        $results = $cigarette_machine->execute($purchase_transaction);

        // THEN
        self::assertEquals(1, $results->getItemQuantity());
        self::assertEquals(4.99, $results->getTotalAmount());

        self::assertEquals(1, $results->getChange()[0][1]);
        self::assertEquals(0, $results->getChange()[1][1]);
    }
}
