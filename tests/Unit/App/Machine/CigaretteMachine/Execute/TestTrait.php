<?php

declare(strict_types=1);

namespace App\Tests\Unit\App\Machine\CigaretteMachine\Execute;

use Mockery as m;
use App\Machine\Contract\PurchaseTransactionInterface;

trait TestTrait
{
    public function validEntryDataProvider(): iterable
    {
        yield '0.01 change to return' => [
            'item_quantity' => 1,
            'paid_amount' => 500,
        ];
    }

    private function mockPurchaseTransaction(int $item_quantity, float $paid_amount): PurchaseTransactionInterface
    {
        $mock = m::mock(PurchaseTransactionInterface::class);
        $mock->allows()->getItemQuantity()->andReturn($item_quantity);
        $mock->allows()->getPaidAmount()->andReturn($paid_amount);

        return $mock;
    }
}
