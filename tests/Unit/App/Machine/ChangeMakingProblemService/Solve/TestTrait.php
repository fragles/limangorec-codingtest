<?php

declare(strict_types=1);

namespace App\Tests\Unit\App\Machine\ChangeMakingProblemService\Solve;

trait TestTrait
{
    public function validEntryDataProvider(): iterable
    {
        yield [
            'coins' => [1, 2, 5, 10, 20, 50, 100, 200],
            'amount' => 64,
            'expected_result' => [0, 2, 0, 1, 0, 1, 0, 0],
        ];

        yield [
            'coins' => [1, 2, 5, 10, 20, 50, 100, 200],
            'amount' => 1,
            'expected_result' => [1, 0, 0, 0, 0, 0, 0, 0],
        ];

        yield [
            'coins' => [1, 2, 5, 10, 20, 50, 100, 200],
            'amount' => 587,
            'expected_result' => [0, 1, 1, 1, 1, 1, 1, 2],
        ];
    }

    public function invalidEntryDataProvider(): iterable
    {
        yield [
            'coins' => [2, 3],
            'amount' => 1,
        ];

        yield [
            'coins' => [2],
            'amount' => 1,
        ];

        yield [
            'coins' => [],
            'amount' => 1,
        ];
    }

    private static function assertResultArrayEquals(array $expected_result, array $result)
    {
        $expected_result_json = \json_encode($expected_result);
        $result_json = \json_encode(array_values($result));

        print_r($expected_result_json);
        print_r($result_json);

        self::assertEquals($expected_result_json, $result_json);
    }
}
