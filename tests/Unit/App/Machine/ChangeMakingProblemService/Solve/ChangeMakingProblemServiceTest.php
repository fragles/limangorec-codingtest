<?php

declare(strict_types=1);

namespace App\Tests\Unit\App\Machine\ChangeMakingProblemService\Solve;

use PHPUnit\Framework\TestCase;
use App\Machine\Exception\ChangeProblemException;
use App\Machine\Service\ChangeMakingProblemGreedyService;

/**
 * @internal
 * @covers \ChangeMakingProblemGreedyService::solve
 */
class ChangeMakingProblemServiceTest extends TestCase
{
    use TestTrait;

    /**
     * @dataProvider validEntryDataProvider
     *
     * @test
     */
    public function provideValidEntryDataGetValidResults(array $coins, int $amount, array $expected_results): void
    {
        // GIVEN
        $service = new ChangeMakingProblemGreedyService();

        // WHEN
        $results = $service->solve($coins, $amount);

        // THEN
        self::assertResultArrayEquals($expected_results, $results);
    }

    /**
     * @dataProvider invalidEntryDataProvider
     *
     * @test
     */
    public function provideInvalidEntryDataGetException(array $coins, int $amount): void
    {
        // GIVEN
        $service = new ChangeMakingProblemGreedyService();

        $this->expectException(ChangeProblemException::class);

        // WHEN
        $results = $service->solve($coins, $amount);

        // THEN
    }
}
